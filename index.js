/*jslint node: true*/
var five = require("johnny-five"),
    board = new five.Board({
      debug: true
    });

board.on('ready', function(){
  var motor = new five.Motor({
    pins: {
      pwm: 3,
      dir: 8,
      cdir: 12,
    }
  })
  var led = new five.Led(13)

  board.repl.inject({
    motor: motor,
    stop: function(){
      motor.stop()
    },
    up: function(){
      motor.forward(64);
    },
    down: function(){
      motor.reverse(64);
  }
  })

  motor.on('start', function(err, timestamp){
    console.log('Started motor', timestamp)
  })

  motor.on('stop', function(err, timestamp){
    console.log('Stopped motor', timestamp)
  })

  motor.on('forward', function(err, timestamp){
    console.log('Clockwise.', timestamp)

    board.wait(500, function(){
      motor.stop();
    })
  })

  motor.on('reverse', function(err, timestamp){
    console.log('Anti-Clockwise.', timestamp)

    board.wait(500, function(){
      motor.stop();
    })
  })

})
